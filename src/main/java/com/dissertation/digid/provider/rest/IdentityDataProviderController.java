package com.dissertation.digid.provider.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/digid-provider")
public class IdentityDataProviderController {

    @GetMapping("/test")
    public String test() {
        return "Hello world from provider";
    }
}
