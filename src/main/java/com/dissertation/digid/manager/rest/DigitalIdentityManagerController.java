package com.dissertation.digid.manager.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/digid-manager")
public class DigitalIdentityManagerController {

    @GetMapping("/test")
    public String test() {
        return "Hello world from manager";
    }
}
